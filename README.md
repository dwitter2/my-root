### I am [g]ROOT?

[![build status](https://git.doit.wisc.edu/ci/projects/15/status.png?ref=master)](https://git.doit.wisc.edu/ci/projects/15?ref=master)

This is the root context of myuw. It is just here to serve up some static html pages for 500 and 400 level errors.

### Development

From the root directory, run `npm start`. The web app will run at [http://localhost:8080](http://localhost:8080) and will 
show the 404 page by default. 